/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      <View style={styles.container}>
          <TouchableOpacity
            style={styles.button}
            onPress={this.onPress}
          >
          <Text style={styles.label}>측정 시작</Text>
          </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#2E364A',
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
    width: 160,
    height: 56,
    borderRadius: 200,
    borderColor : '#8BB1FB',
    backgroundColor: 'rgba(139, 177, 251, 0.06)',
    borderWidth: 1,
  },
  label: {
    width: 120,
    height: 20,
    fontSize: 16,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 23.3,
    letterSpacing: 0,
    color: '#8BB1FB',
    textAlign: 'center',
  }
});
